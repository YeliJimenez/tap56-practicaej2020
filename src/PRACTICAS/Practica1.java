package PRACTICAS;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Practica1 extends JFrame implements ActionListener {

    JTextField texto;

    Practica1() {
        this.setTitle("Saludar");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 150);
        this.setLocationRelativeTo(null);
        this.setLayout(new FlowLayout());

        JLabel panel1 = new JLabel("Escribe un nombre");
        texto = new JTextField(20);
        JButton boton1 = new JButton("¡Saludar!");
        boton1.addActionListener(this);

        this.add(panel1);
        this.add(texto);
        this.add(boton1);

    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica1().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this,"Hola "+texto.getText());
    }

}

